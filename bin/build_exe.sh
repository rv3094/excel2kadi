#!/usr/bin/env bash
cd "$(dirname $(readlink -f $0))/.."

pip3 install -e .[dev]
pyinstaller --noconfirm --clean --onefile --windowed --name excel2kadi app.py
