# excel2kadi

This package provides various conversion tools between Excel and
[Kadi](https://kadi.iam.kit.edu). Using it requires an installation of Python
version **>=3.8**, unless using a pre-built binary (see the last section).

## Installation

To install this package, clone or download this repository, navigate into it
and simply run the following command (note that performing the installation in
a virtual environment is recommended in order to avoid package conflicts):

```bash
pip install .
```

## Usage

Two main ways are provided to use the tool, a command line interface (CLI) and
a graphical user interface (GUI). Note that communicating with Kadi requires an
initial setup of the [kadi-apy](https://kadi-apy.readthedocs.io/en/stable)
library.

### CLI

The CLI offers two different commands to use the functionality of this package,
which should be available automatically after the installation:

```bash
kadi2excel --help
excel2kadi --help
```

### GUI

The GUI provides an interactive way to use the functionality of this package
and can be started by running the `app.py` script:

```bash
python app.py
```

## Pre-built binaries

Alternatively, pre-built binaries for using the GUI (created with
[PyInstaller](https://pyinstaller.org/en/stable)) can be obtained
[here](https://bwsyncandshare.kit.edu/s/wbKsCKe27dgbSGB). Note that these may
not necessarily represent the latest state of this package.

The following two binaries are provided:

* `excel2kadi.exe`: Windows binary, requires Windows version **>=10**
* `excel2kadi`: Linux binary, requires glibc version **>=2.31** (64 Bit)
