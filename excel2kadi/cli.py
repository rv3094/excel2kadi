# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import sys

import click
from kadi_apy import KadiManager

from .excel import excel_to_extras
from .excel import extras_to_excel
from .kadi import create_record
from .kadi import export_template
from .kadi import parse_template
from .kadi import save_extras


@click.command()
@click.option(
    "-o", "--output-path", required=True, help="The path of the output Excel file."
)
@click.option(
    "-p",
    "--template-path",
    help="The path of a template (JSON) input file. Takes precedence over a given"
    " template ID.",
)
@click.option(
    "-t",
    "--template-id",
    help="The ID of a template exported from Kadi to use as input.",
)
@click.option(
    "-i", "--instance", help="The Kadi instance according to the kadi-apy config file."
)
@click.option(
    "-s",
    "--separator",
    default=" : ",
    help="The key separator to use for flattening the metadata entries.",
)
def kadi2excel(output_path, template_path, template_id, instance, separator):
    """Convert Kadi metadata to an Excel sheet."""
    if not template_path and not template_id:
        click.echo("Either a template path or template ID has to be provided.")
        sys.exit(1)

    if template_path:
        click.echo("Loading template via provided path...")
        extras = parse_template(template_path)
    else:
        click.echo("Loading template via provided ID...")

        manager = KadiManager(instance=instance)
        extras = export_template(manager, template_id)

    if extras is None:
        click.echo("Invalid template type.")
        sys.exit(1)

    extras_to_excel(extras, output_path, separator)
    click.echo(f"Excel sheet '{output_path}' created successfully.")


@click.command()
@click.option(
    "-p", "--excel-path", required=True, help="The path of the input Excel file."
)
@click.option(
    "-o",
    "--output-path",
    help="The path of an output file to store the metadata as JSON.",
)
@click.option(
    "-r",
    "--record-identifier",
    help="The identifier of the record to create or update using the metadata. Will"
    " also be used for the title of the record if it is created.",
)
@click.option(
    "-i", "--instance", help="The Kadi instance according to the kadi-apy config file."
)
@click.option(
    "-s",
    "--separator",
    default=" : ",
    help="The key separator that is used for flattening the metadata entries.",
)
def excel2kadi(excel_path, output_path, record_identifier, instance, separator):
    """Convert an Excel sheet to Kadi metadata."""
    if not output_path and not record_identifier:
        click.echo("Either an output path and/or record identifier has to be provided.")
        sys.exit(1)

    extras = excel_to_extras(excel_path, separator)

    if output_path:
        save_extras(output_path, extras)
        click.echo(f"Metadata file '{output_path}' created successfully.")

    else:
        manager = KadiManager(instance=instance)
        record = create_record(manager, record_identifier, update=True, extras=extras)

        click.echo(f"Record with ID '{record.id}' created/updated successfully.")
