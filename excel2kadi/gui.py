# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import os

from kadi_apy import CONFIG_PATH
from kadi_apy import KadiManager
from PySide6.QtCore import QRegularExpression
from PySide6.QtCore import QSettings
from PySide6.QtCore import QUrl
from PySide6.QtCore import Signal
from PySide6.QtGui import QColor
from PySide6.QtGui import QDesktopServices
from PySide6.QtGui import QPalette
from PySide6.QtGui import QRegularExpressionValidator
from PySide6.QtWidgets import QApplication
from PySide6.QtWidgets import QComboBox
from PySide6.QtWidgets import QFileDialog
from PySide6.QtWidgets import QFormLayout
from PySide6.QtWidgets import QFrame
from PySide6.QtWidgets import QGroupBox
from PySide6.QtWidgets import QHBoxLayout
from PySide6.QtWidgets import QLabel
from PySide6.QtWidgets import QLineEdit
from PySide6.QtWidgets import QMessageBox
from PySide6.QtWidgets import QPushButton
from PySide6.QtWidgets import QRadioButton
from PySide6.QtWidgets import QScrollArea
from PySide6.QtWidgets import QTabWidget
from PySide6.QtWidgets import QTextEdit
from PySide6.QtWidgets import QVBoxLayout
from PySide6.QtWidgets import QWidget

from .excel import excel_to_extras
from .excel import extras_to_excel
from .kadi import create_config_file
from .kadi import create_record
from .kadi import export_template
from .kadi import parse_template
from .kadi import save_extras


# Similar to the regular int validator, but ensures that empty strings are also valid so
# events after finishing editing are triggered consistently.
INT_VALIDATOR = QRegularExpressionValidator(QRegularExpression("(^[0-9]+$|^$)"))


def clear_layout(layout):
    if layout is not None:
        while layout.count():
            item = layout.takeAt(0)
            widget = item.widget()

            if widget is not None:
                widget.deleteLater()
            else:
                clear_layout(item.layout())


def show_file_dialog(widget, title, file_ext, is_save_dialog=False):
    dialog_func = (
        QFileDialog.getSaveFileName if is_save_dialog else QFileDialog.getOpenFileName
    )
    filename, _ = dialog_func(widget, "Select a file", "", f"{title} (*{file_ext})")

    if filename:
        if not filename.endswith(file_ext):
            filename += file_ext

        widget.setText(filename)


class BasePalette(QPalette):
    def __init__(self, color="#e4e4e4"):
        super().__init__()

        self.setColor(QPalette.ColorRole.Base, QColor(color))


class HBoxLayout(QHBoxLayout):
    def __init__(self, *widgets):
        super().__init__()

        for widget in widgets:
            self.addWidget(widget)


class FileInputLayout(HBoxLayout):
    def __init__(self, input_widget, on_button_clicked, title="Browse"):
        input_widget.setReadOnly(True)
        input_widget.setPalette(BasePalette())

        browser_button = QPushButton(title)
        browser_button.clicked.connect(on_button_clicked)

        super().__init__(input_widget, browser_button)


class PersistentInputMixin:
    def __init__(self, settings, section, key):
        super().__init__()

        self._settings = settings
        self._settings_key = f"{section}/{key}"

        self.editingFinished.connect(self._persist_text)
        self._restore_text()

    def _get_text(self):
        raise NotImplementedError

    def _set_text(self, text):
        raise NotImplementedError

    def _persist_text(self):
        self._settings.setValue(self._settings_key, self._get_text())

    def _restore_text(self):
        text = self._settings.value(self._settings_key)

        if text:
            self._set_text(text)


class PersistentTextEdit(PersistentInputMixin, QTextEdit):
    editingFinished = Signal()

    def __init__(self, settings, section, key):
        super().__init__(settings, section, key)

        self._changed = False
        self.textChanged.connect(self._on_text_changed)

    def _on_text_changed(self):
        self._changed = True

    def _get_text(self):
        return self.toPlainText()

    def _set_text(self, text):
        self.setText(text)

    def setText(self, text):
        super().setText(text)
        self._persist_text()

    def focusOutEvent(self, event):
        super().focusOutEvent(event)

        if self._changed:
            self.editingFinished.emit()

        self._changed = False


class PersistentLineEdit(PersistentInputMixin, QLineEdit):
    def _get_text(self):
        return self.text()

    def _set_text(self, text):
        self.setText(text)

    def setText(self, text):
        super().setText(text)
        self._persist_text()


class PermissionsWidget(QWidget):
    def __init__(self, settings, section, key):
        super().__init__()

        self._settings = settings
        self._settings_key = f"{section}/{key}"

        self._main_layout = QVBoxLayout()
        self._main_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self._main_layout)

        add_row_button = QPushButton("Add role")
        add_row_button.clicked.connect(self._on_add_row_button_clicked)
        self._main_layout.addWidget(add_row_button)

        self._num_rows = 0
        self._restore_roles()

        if self._num_rows == 0:
            self._add_row()

    def _add_row(self, subject_type=None, subject_id=None, role=None):
        row_layout = QHBoxLayout()

        subject_type_input = QComboBox()
        subject_type_input.addItems(["User", "Group"])

        if subject_type:
            subject_type_input.setCurrentText(subject_type)

        subject_type_input.currentTextChanged.connect(self._persist_roles)
        row_layout.addWidget(subject_type_input)

        subject_id_input = QLineEdit()
        subject_id_input.setPlaceholderText("User or group ID")
        subject_id_input.setValidator(INT_VALIDATOR)

        if subject_id:
            subject_id_input.setText(subject_id)

        subject_id_input.editingFinished.connect(self._persist_roles)
        row_layout.addWidget(subject_id_input)

        role_input = QComboBox()
        role_input.addItems(["Member", "Collaborator", "Editor", "Admin"])

        if role:
            role_input.setCurrentText(role)

        role_input.currentTextChanged.connect(self._persist_roles)
        row_layout.addWidget(role_input)

        remove_role_button = QPushButton("Remove role")
        remove_role_button.clicked.connect(
            lambda: self._on_remove_role_button_clicked(row_layout)
        )
        row_layout.addWidget(remove_role_button)

        self._main_layout.insertLayout(self._num_rows, row_layout)
        self._num_rows += 1

    def _remove_row(self, row_layout):
        self._main_layout.removeItem(row_layout)
        self._num_rows -= 1

        clear_layout(row_layout)

    def _get_role_data(self):
        roles = []

        for i in range(self._main_layout.count() - 1):
            row_layout = self._main_layout.itemAt(i)

            subject_type = row_layout.itemAt(0).widget().currentText()
            subject_id = row_layout.itemAt(1).widget().text()
            role = row_layout.itemAt(2).widget().currentText()

            if subject_id:
                roles.append(
                    {
                        "subject_type": subject_type,
                        "subject_id": subject_id,
                        "role": role,
                    }
                )

        return roles

    def _persist_roles(self):
        roles = self._get_role_data()
        role_data = json.dumps(roles, separators=(",", ":"))
        self._settings.setValue(self._settings_key, role_data)

    def _restore_roles(self):
        role_data = self._settings.value(self._settings_key)

        try:
            roles = json.loads(role_data)
        except:
            roles = []

        for role_meta in roles:
            self._add_row(
                subject_type=role_meta["subject_type"],
                subject_id=role_meta["subject_id"],
                role=role_meta["role"],
            )

    def _on_add_row_button_clicked(self):
        self._add_row()

    def _on_remove_role_button_clicked(self, row_layout):
        self._remove_row(row_layout)
        self._persist_roles()

    def role_data(self):
        roles = self._get_role_data()
        data = []

        for role_meta in roles:
            try:
                subject_id = int(role_meta["subject_id"])
            except:
                continue

            data.append(
                {
                    "subject_type": role_meta["subject_type"].lower(),
                    "subject_id": subject_id,
                    "role": role_meta["role"].lower(),
                }
            )

        return data


class Excel2KadiWidget(QWidget):
    def __init__(self, settings):
        super().__init__()

        self._main_layout = QVBoxLayout()
        self._main_layout.setSpacing(20)
        self.setLayout(self._main_layout)

        self._settings = settings
        self._section = "excel2kadi"
        self._output_type = "kadi"

        self._setup_widgets()

    def _setup_input_settings_widgets(self):
        input_settings_layout = QFormLayout()
        input_settings_group_box = QGroupBox("Input settings")
        input_settings_group_box.setLayout(input_settings_layout)

        self._input_file_input = PersistentLineEdit(
            self._settings, self._section, "input"
        )
        input_settings_layout.addRow(
            "Input file*",
            FileInputLayout(self._input_file_input, self._on_input_file_button_clicked),
        )

        self._separator_input = PersistentLineEdit(
            self._settings, self._section, "separator"
        )
        self._separator_input.setPlaceholderText('Defaults to " : "')
        input_settings_layout.addRow("Key separator", self._separator_input)

        self._main_layout.addWidget(input_settings_group_box)

    def _setup_kadi_output_widgets(self, output_settings_layout):
        kadi_output_layout = QVBoxLayout()
        kadi_output_layout.setSpacing(20)
        kadi_output_layout.setContentsMargins(0, 10, 0, 0)

        self._kadi_output_widget = QWidget()
        self._kadi_output_widget.setLayout(kadi_output_layout)
        self._kadi_output_widget.setVisible(True)
        output_settings_layout.addWidget(self._kadi_output_widget)

        # Basic record metadata.
        metadata_layout = QFormLayout()
        metadata_group_box = QGroupBox("Basic record metadata")
        metadata_group_box.setLayout(metadata_layout)
        kadi_output_layout.addWidget(metadata_group_box)

        self._title_input = PersistentLineEdit(self._settings, self._section, "title")
        metadata_layout.addRow("Title*", self._title_input)

        self._identifier_input = PersistentLineEdit(
            self._settings, self._section, "identifier"
        )
        self._identifier_input.setPlaceholderText(
            "Note that invalid characters will be removed/replaced automatically"
        )
        metadata_layout.addRow("Identifier*", self._identifier_input)

        self._type_input = PersistentLineEdit(self._settings, self._section, "type")
        metadata_layout.addRow("Type", self._type_input)

        self._description_input = PersistentTextEdit(
            self._settings, self._section, "description"
        )
        metadata_layout.addRow("Description", self._description_input)

        self._license_input = PersistentLineEdit(
            self._settings, self._section, "license"
        )
        self._license_input.setPlaceholderText(
            "The short name of a license supported by Kadi"
        )
        metadata_layout.addRow("License", self._license_input)

        self._tags_input = PersistentLineEdit(self._settings, self._section, "tags")
        self._tags_input.setPlaceholderText("Multiple tags can be separated by a comma")
        metadata_layout.addRow("Tags", self._tags_input)

        # Additional record settings.
        settings_layout = QFormLayout()
        settings_group_box = QGroupBox("Additional record settings")
        settings_group_box.setLayout(settings_layout)
        kadi_output_layout.addWidget(settings_group_box)

        self._collections_input = PersistentLineEdit(
            self._settings, self._section, "collections"
        )
        self._collections_input.setPlaceholderText(
            "One or more collection IDs (separated by a comma)"
        )
        settings_layout.addRow("Collections", self._collections_input)

        self._permissions_input = PermissionsWidget(
            self._settings, self._section, "roles"
        )
        settings_layout.addRow("Permissions", self._permissions_input)

        # Kadi instance.
        instance_layout = QFormLayout()
        instance_layout.setSpacing(8)
        kadi_output_layout.addLayout(instance_layout)

        self._instance_input = PersistentLineEdit(
            self._settings, self._section, "instance"
        )
        self._instance_input.setPlaceholderText(
            "Defaults to the instance set in the config file"
        )
        instance_layout.addRow("Kadi instance", self._instance_input)

    def _setup_file_output_widgets(self, output_settings_layout):
        file_output_layout = QFormLayout()
        file_output_layout.setContentsMargins(0, 10, 0, 0)

        self._file_output_widget = QWidget()
        self._file_output_widget.setLayout(file_output_layout)
        self._file_output_widget.setVisible(False)
        output_settings_layout.addWidget(self._file_output_widget)

        self._output_file_input = QLineEdit()
        file_output_layout.addRow(
            "Metadata file*",
            FileInputLayout(
                self._output_file_input, self._on_output_file_button_clicked
            ),
        )

    def _setup_output_settings_widgets(self):
        output_settings_layout = QVBoxLayout()
        output_settings_group_box = QGroupBox("Output settings")
        output_settings_group_box.setLayout(output_settings_layout)

        # Output type selection.
        output_type_layout = QHBoxLayout()
        output_settings_layout.addLayout(output_type_layout)

        kadi_radio_button = QRadioButton("To Kadi record")
        kadi_radio_button.setChecked(True)
        kadi_radio_button.toggled.connect(
            lambda: self._on_radio_button_clicked(kadi_radio_button, "kadi")
        )
        output_type_layout.addWidget(kadi_radio_button)

        file_radio_button = QRadioButton("To file")
        file_radio_button.toggled.connect(
            lambda: self._on_radio_button_clicked(file_radio_button, "file")
        )
        output_type_layout.addWidget(file_radio_button)

        output_type_layout.addStretch(1)

        self._setup_kadi_output_widgets(output_settings_layout)
        self._setup_file_output_widgets(output_settings_layout)

        self._main_layout.addWidget(output_settings_group_box)

    def _setup_widgets(self):
        self._setup_input_settings_widgets()
        self._setup_output_settings_widgets()

        self._main_layout.addStretch(1)

        run_conversion_layout = QHBoxLayout()
        run_conversion_layout.setContentsMargins(0, 10, 0, 0)
        run_conversion_button = QPushButton("Run conversion")
        run_conversion_button.clicked.connect(self._on_run_conversion_button_clicked)
        run_conversion_layout.addWidget(run_conversion_button)

        self._main_layout.addLayout(run_conversion_layout)

    def _on_input_file_button_clicked(self):
        show_file_dialog(self._input_file_input, "Excel", ".xlsx")

    def _on_radio_button_clicked(self, radio_button, output_type):
        if radio_button.isChecked():
            self._output_type = output_type
            self._kadi_output_widget.setVisible(output_type == "kadi")
            self._file_output_widget.setVisible(output_type == "file")

    def _on_output_file_button_clicked(self):
        show_file_dialog(self._output_file_input, "JSON", ".json", is_save_dialog=True)

    def _on_run_conversion_button_clicked(self):
        input_path = self._input_file_input.text()

        if not input_path:
            QMessageBox.critical(self, "Error", "Missing input file path.")
            return

        separator = self._separator_input.text() or " : "

        try:
            extras = excel_to_extras(input_path, separator)
        except Exception as e:
            QMessageBox.critical(self, "Error", str(e))
            return

        if self._output_type == "file":
            metadata_path = self._output_file_input.text()

            if not metadata_path:
                QMessageBox.critical(self, "Error", "Missing metadata file path.")
                return

            try:
                save_extras(metadata_path, extras)
            except Exception as e:
                QMessageBox.critical(self, "Error", str(e))
                return

            QMessageBox.information(
                self,
                "Success",
                f"Metadata file '{metadata_path}' created successfully.",
            )

        else:
            instance = self._instance_input.text().strip() or None
            title = self._title_input.text().strip()
            identifier = self._identifier_input.text().strip()

            if not title or not identifier:
                QMessageBox.critical(self, "Error", "Missing title or identifier.")
                return

            record_type = self._type_input.text().strip() or None
            description = self._description_input.toPlainText()
            license = self._license_input.text().strip() or None
            tags = self._tags_input.text().strip() or []

            if tags:
                tags = tags.split(",")

            collections = self._collections_input.text().strip() or None

            if collections:
                try:
                    collections = [int(c) for c in collections.split(",")]
                except:
                    QMessageBox.critical(self, "Error", "Invalid collection IDs.")
                    return

            roles = self._permissions_input.role_data()

            record_args = {
                "title": title,
                "extras": extras,
                "type": record_type,
                "description": description,
                "license": license,
                "tags": tags,
            }

            try:
                manager = KadiManager(instance=instance)
                record = create_record(
                    manager,
                    identifier,
                    collections=collections,
                    roles=roles,
                    **record_args,
                )
            except Exception as e:
                QMessageBox.critical(self, "Error", str(e))
                return

            if record is None:
                ret = QMessageBox.question(
                    self,
                    "Warning",
                    "A record with that identifier already exists. Do you want to"
                    " update its metadata?",
                )

                if ret == QMessageBox.StandardButton.No:
                    return

                try:
                    record = create_record(
                        manager, identifier, update=True, **record_args
                    )
                except Exception as e:
                    QMessageBox.critical(self, "Error", str(e))
                    return

            kadi_url = record._links["self"].replace("/api", "")
            kadi_url = f'<a href="{kadi_url}">View in Kadi</a>'

            QMessageBox.information(
                self,
                "Success",
                f"Record with ID '{record.id}' created/updated successfully."
                f"<br>{kadi_url}",
            )


class Kadi2ExcelWidget(QWidget):
    def __init__(self, settings):
        super().__init__()

        self._main_layout = QVBoxLayout()
        self._main_layout.setSpacing(20)
        self.setLayout(self._main_layout)

        self._settings = settings
        self._section = "kadi2excel"
        self._input_type = "kadi"

        self._setup_widgets()

    def _setup_kadi_input_widgets(self, input_settings_layout):
        kadi_input_layout = QFormLayout()
        kadi_input_layout.setContentsMargins(0, 10, 0, 0)

        self._kadi_input_widget = QWidget()
        self._kadi_input_widget.setLayout(kadi_input_layout)
        input_settings_layout.addWidget(self._kadi_input_widget)

        self._template_id_input = PersistentLineEdit(
            self._settings, self._section, "template_id"
        )
        self._template_id_input.setValidator(INT_VALIDATOR)
        kadi_input_layout.addRow("Template ID*", self._template_id_input)

        self._instance_input = PersistentLineEdit(
            self._settings, self._section, "instance"
        )
        self._instance_input.setPlaceholderText(
            "Defaults to the instance set in the config file"
        )
        kadi_input_layout.addRow("Kadi instance", self._instance_input)

    def _setup_file_input_widgets(self, input_settings_layout):
        file_input_layout = QFormLayout()
        file_input_layout.setContentsMargins(0, 10, 0, 0)

        self._file_input_widget = QWidget()
        self._file_input_widget.setLayout(file_input_layout)
        self._file_input_widget.setVisible(False)
        input_settings_layout.addWidget(self._file_input_widget)

        self._input_file_input = PersistentLineEdit(
            self._settings, self._section, "input"
        )
        file_input_layout.addRow(
            "Template file*",
            FileInputLayout(self._input_file_input, self._on_input_file_button_clicked),
        )

    def _setup_input_settings_widgets(self):
        input_settings_layout = QVBoxLayout()
        input_settings_group_box = QGroupBox("Input settings")
        input_settings_group_box.setLayout(input_settings_layout)

        # Input type selection.
        input_type_layout = QHBoxLayout()
        input_settings_layout.addLayout(input_type_layout)

        kadi_radio_button = QRadioButton("From Kadi")
        kadi_radio_button.setChecked(True)
        kadi_radio_button.toggled.connect(
            lambda: self._on_radio_button_clicked(kadi_radio_button, "kadi")
        )
        input_type_layout.addWidget(kadi_radio_button)

        file_radio_button = QRadioButton("From file")
        file_radio_button.toggled.connect(
            lambda: self._on_radio_button_clicked(file_radio_button, "file")
        )
        input_type_layout.addWidget(file_radio_button)

        input_type_layout.addStretch(1)

        self._setup_kadi_input_widgets(input_settings_layout)
        self._setup_file_input_widgets(input_settings_layout)

        self._main_layout.addWidget(input_settings_group_box)

    def _setup_output_settings_widgets(self):
        output_settings_layout = QFormLayout()
        output_settings_group_box = QGroupBox("Output settings")
        output_settings_group_box.setLayout(output_settings_layout)

        self._output_file_input = QLineEdit()
        output_settings_layout.addRow(
            "Output file*",
            FileInputLayout(
                self._output_file_input, self._on_output_file_button_clicked
            ),
        )

        self._separator_input = PersistentLineEdit(
            self._settings, self._section, "separator"
        )
        self._separator_input.setPlaceholderText('Defaults to " : "')
        output_settings_layout.addRow("Key separator", self._separator_input)

        self._main_layout.addWidget(output_settings_group_box)

    def _setup_widgets(self):
        self._setup_input_settings_widgets()
        self._setup_output_settings_widgets()

        self._main_layout.addStretch(1)

        run_conversion_layout = QHBoxLayout()
        run_conversion_layout.setContentsMargins(0, 10, 0, 0)
        run_conversion_button = QPushButton("Run conversion")
        run_conversion_button.clicked.connect(self._on_run_conversion_button_clicked)
        run_conversion_layout.addWidget(run_conversion_button)

        self._main_layout.addLayout(run_conversion_layout)

    def _on_radio_button_clicked(self, radio_button, input_type):
        if radio_button.isChecked():
            self._input_type = input_type
            self._kadi_input_widget.setVisible(input_type == "kadi")
            self._file_input_widget.setVisible(input_type == "file")

    def _on_input_file_button_clicked(self):
        show_file_dialog(self._input_file_input, "JSON", ".json")

    def _on_output_file_button_clicked(self):
        show_file_dialog(self._output_file_input, "Excel", ".xlsx", is_save_dialog=True)

    def _on_run_conversion_button_clicked(self):
        output_path = self._output_file_input.text()

        if not output_path:
            QMessageBox.critical(self, "Error", "Missing output file path.")
            return

        if self._input_type == "kadi":
            if not os.path.exists(CONFIG_PATH):
                QMessageBox.information(
                    self, "Information", "Please set up the Kadi config first."
                )
                return

            try:
                template_id = int(self._template_id_input.text())
            except:
                QMessageBox.critical(self, "Error", "Missing or invalid template ID.")
                return

            instance = self._instance_input.text().strip() or None

            try:
                manager = KadiManager(instance=instance)
                extras = export_template(manager, template_id)
            except Exception as e:
                QMessageBox.critical(self, "Error", str(e))
                return

        else:
            template_path = self._input_file_input.text()

            if not template_path:
                QMessageBox.critical(self, "Error", "Missing template file path.")
                return

            try:
                extras = parse_template(template_path)
            except Exception as e:
                QMessageBox.critical(self, "Error", str(e))
                return

        if extras is None:
            QMessageBox.critical(self, "Error", "Invalid template type.")
            return

        separator = self._separator_input.text() or " : "

        try:
            extras_to_excel(extras, output_path, separator)
        except Exception as e:
            QMessageBox.critical(self, "Error", str(e))
            return

        QMessageBox.information(
            self,
            "Success",
            f"Excel sheet '{output_path}' created successfully.",
        )


class KadiConfigWidget(QWidget):
    def __init__(self):
        super().__init__()

        self._main_layout = QVBoxLayout()
        self.setLayout(self._main_layout)

        self._setup_widgets()
        self._main_layout.addStretch(1)

    def _setup_widgets(self):
        kadi_apy_url = (
            "https://kadi-apy.readthedocs.io/en/stable/setup/configuration.html"
        )
        config_info_text = (
            "In order to connect with Kadi, a suitable configuration file is required,"
            " which can be created and edited manually with the help of the buttons"
            f' below. Please see the documentation of the <a href="{kadi_apy_url}">'
            "kadi-apy</a> library for more information about the config file."
        )

        config_info_label = QLabel(config_info_text)
        config_info_label.setContentsMargins(0, 0, 0, 15)
        config_info_label.setOpenExternalLinks(True)
        config_info_label.setWordWrap(True)

        self._main_layout.addWidget(config_info_label)

        create_config_button = QPushButton("Create config file")
        create_config_button.clicked.connect(self._on_create_config_button_clicked)

        self._main_layout.addWidget(create_config_button)

        edit_config_button = QPushButton("Edit config file")
        edit_config_button.clicked.connect(self._on_edit_config_button_clicked)

        self._main_layout.addWidget(edit_config_button)

        config_file_input = QLineEdit(str(CONFIG_PATH))
        config_file_input.setReadOnly(True)
        config_file_input.setPalette(BasePalette())
        config_file_button = QPushButton("Open containing folder")
        config_file_button.clicked.connect(self._on_config_file_button_clicked)
        config_file_layout = HBoxLayout(
            QLabel("Config file path"), config_file_input, config_file_button
        )
        config_file_layout.setContentsMargins(0, 15, 0, 0)

        self._main_layout.addLayout(config_file_layout)

    def _on_create_config_button_clicked(self):
        try:
            file_created = create_config_file()
        except Exception as e:
            QMessageBox.critical(self, "Error", str(e))
            return

        if file_created:
            QMessageBox.information(
                self,
                "Information",
                f"A new config file has been created at '{CONFIG_PATH}'.",
            )
        else:
            QMessageBox.information(self, "Information", "Config file already exists.")

    def _on_edit_config_button_clicked(self):
        if not QDesktopServices.openUrl(QUrl.fromLocalFile(str(CONFIG_PATH))):
            QMessageBox.critical(self, "Error", "Error launching text editor.")

    def _on_config_file_button_clicked(self):
        config_folder = os.path.dirname(CONFIG_PATH)

        if not QDesktopServices.openUrl(QUrl.fromLocalFile(config_folder)):
            QMessageBox.critical(self, "Error", "Error launching file explorer.")


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Excel2Kadi")
        self.resize(1000, 750)

        # Move the widget to the center of the screen.
        widget_geometry = self.frameGeometry()
        center_point = self.screen().availableGeometry().center()
        widget_geometry.moveCenter(center_point)
        self.move(widget_geometry.topLeft())

        self._main_layout = QVBoxLayout()
        self.setLayout(self._main_layout)

        self._settings = QSettings("kit.edu", "excel2kadi")
        self._setup_widgets()

    def _setup_widgets(self):
        tab_widget = QTabWidget()

        # Make the Excel2Kadi widget scrollable.
        excel2kadi_widget = QScrollArea()
        excel2kadi_widget.setWidgetResizable(True)
        excel2kadi_widget.setFrameShape(QFrame.Shape.NoFrame)
        excel2kadi_widget.setBackgroundRole(QPalette.ColorRole.Base)
        excel2kadi_widget.setWidget(Excel2KadiWidget(self._settings))

        tab_widget.addTab(excel2kadi_widget, "Excel2Kadi")
        tab_widget.addTab(Kadi2ExcelWidget(self._settings), "Kadi2Excel")
        tab_widget.addTab(KadiConfigWidget(), "Kadi config")

        self._main_layout.addWidget(tab_widget)


def run_gui(args):
    app = QApplication(args)

    main_window = MainWindow()
    main_window.show()

    return app.exec()
