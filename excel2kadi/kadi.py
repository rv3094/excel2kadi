# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import os
from configparser import ConfigParser

from kadi_apy import CONFIG_PATH
from kadi_apy import KadiAPYRequestError


def create_config_file():
    if os.path.exists(CONFIG_PATH):
        return False

    config = ConfigParser()
    default_instance = "my_instance"

    config["global"] = {
        "default": default_instance,
    }
    config[default_instance] = {
        "host": "https://kadi4mat.iam.kit.edu",
        "pat": "",
    }

    with open(CONFIG_PATH, "w", encoding="utf-8") as f:
        config.write(f)

    os.chmod(CONFIG_PATH, 0o600)

    return True


def export_template(manager, template_id):
    template = manager.template(id=template_id)

    if template.meta["type"] == "record":
        return template.meta["data"]["extras"]

    if template.meta["type"] == "extras":
        return template.meta["data"]

    return None


def parse_template(template_path):
    with open(template_path, encoding="utf-8") as f:
        template = json.load(f)

    if template["type"] == "record":
        return template["data"]["extras"]

    if template["type"] == "extras":
        return template["data"]

    return None


def create_record(
    manager, identifier, update=False, collections=None, roles=None, **metadata
):
    collections = collections if collections is not None else []
    roles = roles if roles is not None else []

    record = manager.record(identifier=identifier, create=True, **metadata)

    if record.created:
        for collection in collections:
            record.add_collection_link(collection)

        for role_meta in roles:
            subject_id = role_meta["subject_id"]
            role = role_meta["role"]

            if role_meta["subject_type"] == "user":
                record.add_user(subject_id, role)
            else:
                record.add_group_role(subject_id, role)

    else:
        if not update:
            return None

        response = record.edit(**metadata)

        if not response.ok:
            raise KadiAPYRequestError(response.json())

    return record


def save_extras(extras_path, extras):
    with open(extras_path, mode="w", encoding="utf-8") as f:
        json.dump(extras, f, indent=2, sort_keys=True)
