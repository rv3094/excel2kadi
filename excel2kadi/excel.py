# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import datetime
from datetime import timezone

from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.styles import Border
from openpyxl.styles import Font
from openpyxl.styles import PatternFill
from openpyxl.styles import Side
from openpyxl.styles.numbers import FORMAT_DATE_DATETIME
from openpyxl.styles.numbers import FORMAT_NUMBER
from openpyxl.styles.numbers import FORMAT_NUMBER_00
from openpyxl.styles.numbers import FORMAT_TEXT
from openpyxl.utils.cell import get_column_letter
from openpyxl.worksheet.datavalidation import DataValidation


TYPE_MAPPING = {
    # Short version to user-readable one.
    "str": "String",
    "int": "Integer",
    "float": "Float",
    "bool": "Boolean",
    "date": "Date",
    "dict": "Dictionary",
    "list": "List",
    # User-readable version to short one.
    "String": "str",
    "Integer": "int",
    "Float": "float",
    "Boolean": "bool",
    "Date": "date",
    "Dictionary": "dict",
    "List": "list",
}

NESTED_TYPES = {"dict", "list"}
NUMERIC_TYPES = {"int", "float"}


def _convert_extra_value(extra_type, value):
    if value is None:
        return None

    try:
        if extra_type == "int":
            return int(value)

        if extra_type == "float":
            # We also try to accept commas as delimiters.
            if isinstance(value, str):
                value = value.replace(",", ".")

            return float(value)

        if extra_type == "bool":
            if value.lower() == "true":
                return True

            return False

        if extra_type == "date":
            local_timezone = datetime.now().astimezone().tzinfo
            return (
                value.replace(tzinfo=local_timezone)
                .astimezone(timezone.utc)
                .isoformat()
            )
    except:
        return None

    return value


def _find_extra(extras, keys):
    key = keys[0]

    for index, extra in enumerate(extras):
        if extra.get("key", str(index + 1)) == key:
            if len(keys) > 1:
                return _find_extra(extra["value"], keys[1:])

            return extra

    return None


def _find_data_validation(worksheet, cell_id):
    for dv in worksheet.data_validations.dataValidation:
        if dv.sqref == cell_id:
            return dv

    return None


def excel_to_extras(excel_path, separator):
    workbook = load_workbook(excel_path)
    worksheet = workbook.active

    extras = []

    for index, row in enumerate(worksheet.rows):
        # Skip the first row.
        if index == 0:
            continue

        # The current layer that new extras should be appended to.
        current_extras = extras

        values = list(cell.value for cell in row)

        extra_type = TYPE_MAPPING[values[0]]
        keys = values[1].split(separator)
        value = values[2] if len(values) > 2 else None
        unit = values[3]

        extra = {
            "type": extra_type,
        }

        # Add the term, if applicable.
        if len(values) > 4:
            term = values[4]

            if term is not None:
                extra["term"] = term

        # Add the value and unit.
        if extra_type in NESTED_TYPES:
            extra["value"] = []
        else:
            if extra_type in NUMERIC_TYPES:
                extra["unit"] = unit

            extra["value"] = _convert_extra_value(extra_type, value)

        # Add the key, depending on whether we are within a list or not, and also
        # determine the current extra layer.
        parent_keys = keys[:-1]

        if len(parent_keys) > 0:
            parent_extra = _find_extra(extras, parent_keys)

            if parent_extra["type"] != "list":
                extra["key"] = keys[-1]

            current_extras = parent_extra["value"]
        else:
            extra["key"] = keys[0]

        # Add the validation.
        extra["validation"] = {}

        # We could check any side for the required validation.
        if worksheet[f"C{index + 1}"].border.left.style is not None:
            extra["validation"]["required"] = True

        dv = _find_data_validation(worksheet, f"C{index + 1}")

        if dv is not None:
            if dv.type == "list" and extra_type != "bool":
                options = dv.formula1.strip('"').split(",")
                options = [_convert_extra_value(extra_type, opt) for opt in options]

                extra["validation"]["options"] = options

            elif dv.type == "decimal":
                min_value = _convert_extra_value(extra_type, dv.formula1)
                max_value = _convert_extra_value(extra_type, dv.formula2)

                if dv.operator == "lessThanOrEqual":
                    min_value = None
                elif dv.operator == "greaterThanOrEqual":
                    max_value = None

                extra["validation"]["range"] = {"min": min_value, "max": max_value}

        if not extra["validation"]:
            del extra["validation"]

        current_extras.append(extra)

    workbook.close()

    return extras


def _extras_include_term(extras):
    for extra in extras:
        if "term" in extra or (
            extra["type"] in NESTED_TYPES and _extras_include_term(extra["value"])
        ):
            return True

    return False


def _extras_to_rows(extras, separator, key_prefix=None):
    rows = []

    for index, extra in enumerate(extras):
        key = extra.get("key", index + 1)

        if key_prefix is not None:
            key = f"{key_prefix}{separator}{key}"

        row = {
            "type": TYPE_MAPPING[extra["type"]],
            "key": key,
            "value": "",
            "unit": extra.get("unit", "") or "",
            "term": extra.get("term", ""),
            "required": False,
            "validation": [],
        }

        # Handle the value.
        if extra["type"] not in NESTED_TYPES and extra["value"] is not None:
            value = extra["value"]

            # Convert booleans to strings.
            if extra["type"] == "bool":
                value = str(value).upper()

            # Convert dates to naive datetime objects using the date and time of the
            # local timezone.
            if extra["type"] == "date":
                value = datetime.fromisoformat(value)
                local_timezone = value.astimezone().tzinfo
                value = value.astimezone(local_timezone).replace(tzinfo=None)

            row["value"] = value

        # Handle validation for boolean values.
        if extra["type"] == "bool":
            options = ",".join(["TRUE", "FALSE"])
            dv = DataValidation(
                type="list", formula1=f'"{options}"', showErrorMessage=True
            )

            row["validation"].append(dv)

        # Handle user-defined validation instructions.
        validation = extra.get("validation", {})

        if "options" in validation:
            options = [str(opt) for opt in validation["options"]]
            options = ",".join(options)

            dv = DataValidation(
                type="list", formula1=f'"{options}"', showErrorMessage=True
            )

            row["validation"].append(dv)

        # Only apply the range validation if no options are given, as multiple
        # validations do not seem to work
        elif "range" in validation:
            min_value = validation["range"]["min"]
            max_value = validation["range"]["max"]

            operator = "between"
            prompt = f"Maximum: {max_value}, Minimum: {min_value}"

            if min_value is None:
                operator = "lessThanOrEqual"
                prompt = f"Maximum: {max_value}"
            elif max_value is None:
                operator = "greaterThanOrEqual"
                prompt = f"Minimum: {min_value}"

            dv = DataValidation(
                type="decimal",
                operator=operator,
                formula1=min_value,
                formula2=max_value,
                promptTitle="Value Range",
                prompt=prompt,
                showInputMessage=True,
                showErrorMessage=True,
            )

            row["validation"].append(dv)

        # Keep the required validation separately, as there is no suitable data
        # validation.
        if "required" in validation:
            row["required"] = True

        rows.append(row)

        if extra["type"] in NESTED_TYPES:
            rows += _extras_to_rows(extra["value"], separator, key_prefix=key)

    return rows


def extras_to_excel(extras, excel_path, separator):
    workbook = Workbook()
    worksheet = workbook.active

    include_term = _extras_include_term(extras)

    header_row = ["Type", "Key", "Value", "Unit"]
    col_widths = {"A": 5, "B": 5, "C": 30, "D": 10}

    if include_term:
        header_row.append("Term")
        col_widths["E"] = 5

    # Add the header row.
    worksheet.append(header_row)
    worksheet.freeze_panes = "A2"

    for cell in worksheet["1"]:
        cell.font = Font(name="Calibri", bold=True)

    # Add the data rows.
    rows = _extras_to_rows(extras, separator)

    for row_index, row in enumerate(rows):
        row_data = [row["type"], row["key"], row["value"], row["unit"]]

        if include_term:
            row_data.append(row["term"])

        worksheet.append(row_data)

        # Calculate an estimate for the column widths.
        for col_index, col in enumerate(row_data):
            col_letter = get_column_letter(col_index + 1)
            col_widths[col_letter] = max(col_widths[col_letter], len(str(col)))

        value_cell = worksheet[f"C{row_index + 2}"]
        extra_type = TYPE_MAPPING[row["type"]]

        # Highlight required values with a border.
        if row["required"]:
            side = Side(border_style="thin", color="FF000000")
            value_cell.border = Border(top=side, right=side, bottom=side, left=side)

        # Highlight cells to fill.
        if row_index % 2 == 0:
            color = "00E0E0E0"
        else:
            color = "00D7D7D7"

        if extra_type not in NESTED_TYPES:
            value_cell.fill = PatternFill("solid", start_color=color, end_color=color)

        if extra_type in NUMERIC_TYPES:
            unit_cell = worksheet[f"D{row_index + 2}"]
            unit_cell.fill = PatternFill("solid", start_color=color, end_color=color)

        # Use the correct number format depending on the type.
        if extra_type in {"str", "bool"}:
            value_cell.number_format = FORMAT_TEXT
        elif extra_type == "int":
            value_cell.number_format = FORMAT_NUMBER
        elif extra_type == "float":
            value_cell.number_format = FORMAT_NUMBER_00
        elif extra_type == "date":
            value_cell.number_format = FORMAT_DATE_DATETIME

        # Add the data validation to the sheet.
        for validation in row["validation"]:
            worksheet.add_data_validation(validation)
            validation.add(value_cell)

    # Apply the calculated column dimensions.
    for letter, width in col_widths.items():
        worksheet.column_dimensions[letter].width = width

    workbook.save(excel_path)
